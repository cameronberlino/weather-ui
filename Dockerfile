FROM node:8.10

ENV HOME=/home/app

COPY . $HOME/weather-ui/

WORKDIR $HOME/weather-ui/

RUN npm install

USER root

CMD [ "npm", "run", "start" ]