import axios from "axios";

export default class Geolocation {
  key = "AIzaSyANVirezEmSYk-ahC16yf9ej39UElY1rAc";
  location = "geocode";
  timezone = "timezone";

  getBaseUrl(command) {
    return `https://maps.googleapis.com/maps/api/${command}/json?key=${
      this.key
    }&`;
  }

  async getLocationByZipCode(zipCode) {
    try {
      let {
        data: { results }
      } = await axios({
        method: "get",
        url: `${this.getBaseUrl(this.location)}address=${zipCode}`
      });

      if (typeof results[results.length - 5] !== "undefined") {
        results = results[results.length - 5];
      } else {
        results = results.pop();
      }

      return results;
    } catch (e) {
      console.log(e);
    }
  }

  async getTimeZoneByLatAndLong(latitude, longitude) {
    try {
      let {
        data: { timeZoneId }
      } = await axios({
        method: "get",
        url: `${this.getBaseUrl(
          this.timezone
        )}location=${latitude},${longitude}&timestamp=${Math.ceil(
          new Date().getTime() / 1000
        )}`
      });

      return timeZoneId;
    } catch (e) {
      console.log(e);
      return {};
    }
  }
}
