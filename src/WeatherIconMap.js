export const WeatherIconMap = {
  "clear-day": "https://darksky.net/images/weather-icons/clear-day.png",
  "clear-night": "https://darksky.net/images/weather-icons/clear-night.png",
  rain: "https://darksky.net/images/weather-icons/rain.png",
  snow: "https://darksky.net/images/weather-icons/snow.png",
  sleet: "https://darksky.net/images/weather-icons/sleet.png",
  wind: "https://darksky.net/images/weather-icons/wind.png",
  fog: "https://darksky.net/images/weather-icons/fog.png",
  cloudy: "https://darksky.net/images/weather-icons/cloudy.png",
  "partly-cloudy-day":
    "https://darksky.net/images/weather-icons/partly-cloudy-day.png",
  "partly-cloudy-night":
    "https://darksky.net/images/weather-icons/partly-cloudy-night.png"
};
