import React, { Component } from "react";
import "./App.scss";
import Location from "./components/location";
import Days from "./components/days";
import Geolocation from "./Geolocation";

class App extends Component {
  geolocation = new Geolocation();

  constructor(props) {
    super(props);
    this.state = {
      address: "",
      lat: "",
      long: "",
      date: "",
      description: "",
      low: "",
      sunriseTime: "",
      sunsetTime: "",
      humidity: "",
      pressure: "",
      icon: "",
      timeZone: ""
    };

    this.child = React.createRef();
  }

  async getDefaultLocation() {
    return this.geolocation.getLocationByZipCode(92504);
  }

  async componentDidMount() {
    let location;

    try {
      location = await this.getDefaultLocation();

      let timeZone = await this.geolocation.getTimeZoneByLatAndLong(
        location.geometry.location.lat,
        location.geometry.location.lng
      );

      this.setState({
        address: location.formatted_address,
        lat: location.geometry.location.lat,
        long: location.geometry.location.lng,
        timeZone: timeZone
      });
    } catch (e) {
      console.log(e);
    }
  }

  dayWasClicked(props) {
    this.setState({
      date: props.date,
      description: props.summary,
      low: props.lowTemp,
      high: props.highTemp,
      sunriseTime: props.sunriseTime,
      sunsetTime: props.sunsetTime,
      humidity: props.humidity,
      pressure: props.pressure,
      icon: props.icon
    });
  }

  async zipWasChanged(zip) {
    try {
      let location = await this.geolocation.getLocationByZipCode(zip);
      let timeZone = await this.geolocation.getTimeZoneByLatAndLong(
        location.geometry.location.lat,
        location.geometry.location.lng
      );

      this.setState({
        address: location.formatted_address,
        lat: location.geometry.location.lat,
        long: location.geometry.location.lng,
        timeZone: timeZone
      });

      await this.child.current.callWeatherApi();
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    let address;
    let location;

    if (this.state.address) {
      address = (
        <div id="location">
          <Location
            address={this.state.address}
            date={this.state.date}
            description={this.state.description}
            low={this.state.low}
            high={this.state.high}
            sunriseTime={this.state.sunriseTime}
            sunsetTime={this.state.sunsetTime}
            humidity={this.state.humidity}
            pressure={this.state.pressure}
            icon={this.state.icon}
            timeZone={this.state.timeZone}
            zipChanged={this.zipWasChanged.bind(this)}
          />
        </div>
      );
    }

    if (this.state.lat) {
      location = (
        <Days
          ref={this.child}
          lat={this.state.lat}
          long={this.state.long}
          timeZone={this.state.timeZone}
          callParent={this.dayWasClicked.bind(this)}
        />
      );
    }

    return (
      <div id="weather" className="container">
        {address}
        {location}
      </div>
    );
  }
}

export default App;
