import React, { Component } from "react";
import Day from "../day";
import axios from "axios";
import styles from "./style.module.scss";

class Days extends Component {
  constructor(props) {
    super(props);
    this.state = { days: [] };
    this.child = React.createRef();
  }

  async callWeatherApi() {
    const {
      data: { days }
    } = await axios({
      method: "get",
      url: `http://35.192.235.118/weather/previousweek?latitude=${
        this.props.lat
      }&longitude=${this.props.long}`
    });

    this.setState({ days });
    this.child.current.getLastDay();
  }

  async componentDidMount() {
    await this.callWeatherApi();
  }

  onLoad(props) {
    this.dayWasClicked(props);
  }

  dayWasClicked(props) {
    this.props.callParent(props);
    window.scrollTo(0, 0);
  }

  render() {
    let days = this.state.days.map((day, index) => (
      <Day
        key={index}
        index={index}
        date={day.daily.data[0].time}
        highTemp={day.daily.data[0].temperatureHigh}
        lowTemp={day.daily.data[0].temperatureLow}
        icon={day.daily.data[0].icon}
        summary={day.daily.data[0].summary}
        sunriseTime={day.daily.data[0].sunriseTime}
        sunsetTime={day.daily.data[0].sunsetTime}
        humidity={day.daily.data[0].humidity}
        pressure={day.daily.data[0].pressure}
        timeZone={this.props.timeZone}
        callParent={this.dayWasClicked.bind(this)}
        onLoad={this.onLoad.bind(this)}
        ref={this.child}
      />
    ));

    return (
      <div className={styles.container}>
        <div id="days" className="row">
          {days}
        </div>
      </div>
    );
  }
}

export default Days;
