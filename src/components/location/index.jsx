import React, { Component } from "react";
import styles from "./style.module.scss";
import moment from "moment-timezone";
import { WeatherIconMap } from "../../WeatherIconMap";

class Location extends Component {
  constructor(props) {
    super(props);
    this.state = { zip: "" };
  }

  zipChange(e) {
    this.setState({ zip: e.target.value });
  }

  onEnter(e) {
    if (e.key === "Enter") {
      this.search();
    }
  }

  search() {
    this.props.zipChanged(this.state.zip);
  }

  render() {
    let details;

    if (this.props.description && this.props.high) {
      details = (
        <div>
          <div className="row">
            <div className="col-sm">
              <div id={styles.date}>
                {moment(this.props.date * 1000)
                  .tz(this.props.timeZone)
                  .format("ddd, Do YYYY")}
              </div>
              <div id="description">{this.props.description}</div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm">
              <div id={styles.weatherInfo}>
                <div id={styles.temperature}>
                  {Math.ceil(this.props.high)}&deg;
                </div>
              </div>
            </div>
            <div id={styles.icon} className="col-sm">
              <img
                src={WeatherIconMap[this.props.icon]}
                alt={this.props.icon}
              />
            </div>
            <div id={styles.data} className="col-sm">
              <p>
                Sunrise:{" "}
                {moment(this.props.sunriseTime * 1000)
                  .tz(this.props.timeZone)
                  .format("h:mm A")}
              </p>
              <p>
                Sunset:{" "}
                {moment(this.props.sunsetTime * 1000)
                  .tz(this.props.timeZone)
                  .format("h:mm A")}
              </p>
              <p>Humidity: {Math.ceil(this.props.humidity * 100)}%</p>
              <p>Pressure: {this.props.pressure} MB</p>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div id={styles.location}>
        <div className="row">
          <div className="col-sm">
            <div id={styles.address}>
              <p>{this.props.address}</p>
              <div id={styles.search} className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Zip Code"
                  aria-label="Zip Code"
                  aria-describedby="search-zip"
                  onChange={e => this.zipChange(e)}
                  onKeyPress={e => this.onEnter(e)}
                />
                <div className="input-group-append">
                  <button
                    className="btn btn-outline-secondary"
                    type="button"
                    id="search-zip"
                    onClick={() => this.search()}
                  >
                    Search
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {details}
        <div id={styles.pastWeek}>
          <p>Weather from the past week</p>
        </div>
      </div>
    );
  }
}

export default Location;
