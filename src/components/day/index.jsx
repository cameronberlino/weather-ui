import React, { Component } from "react";
import moment from "moment-timezone";
import styles from "./style.module.scss";
import { WeatherIconMap } from "../../WeatherIconMap";

class Day extends Component {
  callParent() {
    this.props.callParent(this.props);
  }

  getLastDay() {
    if (6 === this.props.index) {
      this.props.onLoad(this.props);
    }
  }

  componentDidMount() {
    this.getLastDay();
  }

  render() {
    return (
      <div className={styles.day + " col-sm"} onClick={() => this.callParent()}>
        <div className="day-of-week">
          <p className="font-weight-bold">
            {moment(this.props.date * 1000)
              .tz(this.props.timeZone)
              .format("ddd, Do")}
          </p>
        </div>
        <div className={styles.icon}>
          <img src={WeatherIconMap[this.props.icon]} alt={this.props.icon} />
        </div>
        <div className={styles.temperature}>
          <span className={styles.lowTemp}>
            {Math.ceil(this.props.lowTemp)}&deg;
          </span>{" "}
          |{" "}
          <span className={styles.highTemp}>
            {Math.ceil(this.props.highTemp)}&deg;
          </span>
        </div>
        <div className={styles.summary}>{this.props.summary}</div>
      </div>
    );
  }
}

export default Day;
